<!DOCTYPE html>
<!--if lt IE 7html.no-js.lt-ie9.lt-ie8.lt-ie7(lang='es')  
-->
<!--if IE 7html.no-js.lt-ie9.lt-ie8(lang='es')  
-->
<!--if IE 8html.no-js.lt-ie9(lang='es')  
-->
<!-- [if gt IE 8] <!-->
<html lang="es" class="no-js">
    <!-- <![endif]-->
    <!-- Insert head html-->
    <head>
        <meta charset="utf-8">
        <!-- SEO-->
        <meta name="description" content="@yield('description-meta')"/>
        <meta name="author" content="Collet Pay">
        <!-- style app -->
        <meta name="msapplication-TileColor" content="#f77800">
        <meta name="theme-color" content="#f77800">
        <meta name="apple-mobile-web-app-status-bar-style" content="#f77800">
        <title>Store - @yield('title')</title>
        <!--<title>Plataforma de promociones International - @yield('title')</title>-->
        <!-- Add meta tags to all project -->
        @yield('aditional-meta')
        <link rel="manifest" href="/manifest.json">
        <!-- Css apply to all project (plugins) -->
        @yield('aditional-css')
        <link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,900" rel="stylesheet">
        <link rel="stylesheet" href="/fonts/icomoon/style.css">
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/jquery-ui.css">
        <link rel="stylesheet" href="/css/owl.carousel.min.css">
        <link rel="stylesheet" href="/css/owl.theme.default.min.css">
        <link rel="stylesheet" href="/css/owl.theme.default.min.css">
        <link rel="stylesheet" href="/css/jquery.fancybox.min.css">
        <link rel="stylesheet" href="/css/bootstrap-datepicker.css">
        <link rel="stylesheet" href="/fonts/flaticon/font/flaticon.css">
        <link rel="stylesheet" href="/css/aos.css">
        <link rel="stylesheet" href="/css/style.css">
    </head>

    <body class="@yield('class-body')">
        <nav class="navbar navbar-light bg-light static-top" role="navigation" style="margin-bottom: 0">
            @include('base.header')
        </nav>
        <div class="content">
            @include('base.main_content')
        </div>
        <footer class="site-footer">
            @include('base.footer')
        </footer>
        @include('base.modal')
        <script src="/js/jquery-3.3.1.min.js"></script>
        <script src="/js/jquery-migrate-3.0.1.min.js"></script>
        <script src="/js/jquery-ui.js"></script>
        <script src="/js/popper.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/owl.carousel.min.js"></script>
        <script src="/js/jquery.stellar.min.js"></script>
        <script src="/js/jquery.countdown.min.js"></script>
        <script src="/js/bootstrap-datepicker.min.js"></script>
        <script src="/js/jquery.easing.1.3.js"></script>
        <script src="/js/aos.js"></script>
        <script src="/js/jquery.fancybox.min.js"></script>
        <script src="/js/jquery.sticky.js"></script>
        <script src="/js/mustache.min.js"></script>
        <script src="/js/custom.js"></script>
        <!-- Js apply to all project (plugins) -->
        @yield('aditional-js')
    </body>
</html>