@extends('base.base_layout', [
'header_anonymous'  => 1,
'header_auth'       => 0,
'menu_main'         => 1,
'aside_left'        => 0,
'aside_right'       => 0,
'layout_bottom'     => 0,
'layout_bottom_a'   => 0,
'layout_bottom_b'   => 0,
'layout_bottom_c'   => 0,
'modal_count'       => 0,
])
@section('class-html', '')
@section('title', 'Tienda Online')
@section('description-meta', '')
@section('aditional-meta')
<!-- Add meta tags to this page -->
@stop
@section('aditional-css')
<!-- Add link tags to this page -->
@stop
@section('class-body', 'page-contact')
@section('content')
<div class="container">
    <div class="row mb-5 justify-content-center">
        <div class="col-md-6 text-center">
            <h2 class="section-title mb-3">Our Products</h2>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae nostrum natus excepturi fuga ullam accusantium vel ut eveniet aut consequatur laboriosam ipsam.</p>
        </div>
    </div>
    <!-- /.row -->
    <div class="row product-list">
        @foreach ($products as $product)
        <div class="col-lg-4 col-md-6 mb-5 element{{ $product->id }}">
            <div class="product-item">
                <figure>
                    <img src="{{ $product->image }}" alt="{{ $product->name }}" class="img-fluid">
                </figure>
                <div class="px-4">
                    <h3>{{ $product->name }}</h3>
                    <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing.{{ $product->description }}</p>
                    <div class="justify-content-center h5 text-black">${{ $product->price }}</div>
                    <div class="custom-control-inline add-cart">
                        @if(array_key_exists($product->id, $cart))
                        <div class='alert alert-success' role='alert'>Adicionado al carrito</div>
                        @else
                        @if(!$product->quantity)
                        <div class='alert alert-danger' role='alert'>Agotado</div>
                        @else
                        <a href="#" class="btn minus">-</a>
                        <input class="form-control quantity{{ $product->id }} text-center" data-max="{{$product->quantity}}" value="1">
                        <a href="#" class="btn plus">+</a>
                        <a href="#" class="btn btn-black ml-1 rounded-0 add" data-element="{{ $product->id }}">Cart</a>
                        @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="row mb-5 justify-content-center">
        {{ $products->links('base.paginator') }}
    </div>
</div>
<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
@stop
<!-- If this page has the variable modal_count > 0,
then each modal content is identified with a number (#)-->
@section('modal_content_#')
<!-- Content of modal number # in this page -->
@stop