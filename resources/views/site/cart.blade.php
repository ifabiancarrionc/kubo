@extends('base.base_layout', [
'header_anonymous'  => 1,
'header_auth'       => 0,
'menu_main'         => 0,
'aside_left'        => 0,
'aside_right'       => 0,
'layout_bottom'     => 0,
'layout_bottom_a'   => 0,
'layout_bottom_b'   => 0,
'layout_bottom_c'   => 0,
'modal_count'       => 0,
])
@section('class-html', '')
@section('title', 'Carrito de Compras')
@section('description-meta', '')
@section('aditional-meta')
<!-- Add meta tags to this page -->
@stop
@section('aditional-css')
<!-- Add link tags to this page -->
@stop
@section('class-body', 'page-contact')
@section('content')
@if($message)
<div class='alert alert-danger' role='alert'>{!! $message !!}</div>
@endif
<div class="container">
    <div class="row mb-5 justify-content-center">
        <div class="col-md-6 text-center">
            <h2 class="section-title mb-3">Carrito de Compras</h2>
        </div>
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Cantidad</th>
                        <th>Valor Unitario</th>
                        <th>Valor Total</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($cart['products'] as $product)
                    <tr class="gradeX element" id="element{{ $product['data']['id'] }}">
                        <td>{{ $product['data']['name'] }}</td>
                        <td>
                            <div class="custom-control-inline">
                                <a href="#" class="btn minus">-</a>
                                <input class="form-control quantity text-center" data-element="{{ $product['data']['id'] }}" data-max="{{$product['data']['quantity']}}" value="{{$product['quantity']}}">
                                <a href="#" class="btn plus">+</a>
                            </div>
                        </td>
                        <td id="price{{ $product['data']['id'] }}">{{ $product['data']['price'] }}</td>
                        <td id="total{{ $product['data']['id'] }}">{{ $product['data']['price']*$product['quantity'] }}</td>
                        <td><a class="delete" data-element="{{ $product['data']['id'] }}" href="#"><i class='icon-trash'></i></a></td>
                    </tr>
                    @endforeach
                    <tr class="gradeX">
                        <td colspan="3">Total</td>
                        <td id="total">{{ $cart['total'] }}</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            <a class="btn btn-primary" href="/">Volver al listado</a>
            @if(count($cart['products'] ))
            <a class="btn btn-primary clear" href="#">Cancelar Compra</a>
            <a class="btn btn-primary confirm" href="#">Confirmar Compra</a>
            @endif
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
@stop
<!-- If this page has the variable modal_count > 0,
then each modal content is identified with a number (#)-->
@section('modal_content_#')
<!-- Content of modal number # in this page -->
@stop