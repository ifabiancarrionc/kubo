<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Site\HomeController@index');
Route::get('/products', function () {
    return App\Models\Product::paginate(9);
});
Route::post('api/v1/cart/add', 'Api\CartController@addElement');
Route::post('api/v1/cart/review', 'Api\CartController@review');
Route::get('/cart', 'Site\HomeController@cart');
Route::put('api/v1/cart/update', 'Api\CartController@updateElement');
Route::delete('api/v1/cart/delete', 'Api\CartController@deleteElement');
Route::delete('api/v1/cart/clear', 'Api\CartController@clear');
Route::post('api/v1/cart/confirm', 'Api\CartController@confirm');