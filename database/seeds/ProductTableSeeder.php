<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product')->insert([
            'id' => 1,
            'name' => "Producto 1",
            'price' => "72000",
            'description' => "",
            'image' => "/image/product.png",
            'quantity' => 10,
            'category_id' => 1,
        ]);
        DB::table('product')->insert([
            'id' => 2,
            'name' => "Producto 2",
            'price' => "80000",
            'description' => "",
            'image' => "/image/product.png",
            'quantity' => 5,
            'category_id' => 2,
        ]);
        DB::table('product')->insert([
            'id' => 3,
            'name' => "Producto 3",
            'price' => "81000",
            'description' => "",
            'image' => "/image/product.png",
            'quantity' => 12,
            'category_id' => 3,
        ]);
        DB::table('product')->insert([
            'id' => 4,
            'name' => "Producto 4",
            'price' => "77000",
            'description' => "",
            'image' => "/image/product.png",
            'quantity' => 8,
            'category_id' => 1,
        ]);
        DB::table('product')->insert([
            'id' => 5,
            'name' => "Producto 5",
            'price' => "42000",
            'description' => "",
            'image' => "/image/product.png",
            'quantity' => 7,
            'category_id' => 2,
        ]);
        DB::table('product')->insert([
            'id' => 6,
            'name' => "Producto 6",
            'price' => "18000",
            'description' => "",
            'image' => "/image/product.png",
            'quantity' => 0,
            'category_id' => 3,
        ]);
        DB::table('product')->insert([
            'id' => 7,
            'name' => "Producto 7",
            'price' => "44000",
            'description' => "",
            'image' => "/image/product.png",
            'quantity' => 13,
            'category_id' => 1,
        ]);
        DB::table('product')->insert([
            'id' => 8,
            'name' => "Producto 8",
            'price' => "35000",
            'description' => "",
            'image' => "/image/product.png",
            'quantity' => 8,
            'category_id' => 2,
        ]);
        DB::table('product')->insert([
            'id' => 9,
            'name' => "Producto 9",
            'price' => "18000",
            'description' => "",
            'image' => "/image/product.png",
            'quantity' => 0,
            'category_id' => 3,
        ]);
        DB::table('product')->insert([
            'id' => 10,
            'name' => "Producto 10",
            'price' => "39000",
            'description' => "",
            'image' => "/image/product.png",
            'quantity' => 18,
            'category_id' => 1,
        ]);
        DB::table('product')->insert([
            'id' => 11,
            'name' => "Producto 11",
            'price' => "70000",
            'description' => "",
            'image' => "/image/product.png",
            'quantity' => 10,
            'category_id' => 2,
        ]);
    }
}
