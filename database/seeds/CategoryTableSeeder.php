<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')->insert([
            'id' => 1,
            'name' => "Categoria 1",
        ]);
        DB::table('category')->insert([
            'id' => 2,
            'name' => "Categoria 1",
        ]);
        DB::table('category')->insert([
            'id' => 3,
            'name' => "Categoria 1",
        ]);
    }
}
