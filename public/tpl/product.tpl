{{#data}}
<div class="col-lg-4 col-md-6 mb-5 element{{ id }}">
    <div class="product-item">
        <figure>
            <img src="{{ image }}" alt="{{ name }}" class="img-fluid">
        </figure>
        <div class="px-4">
            <h3>{{ name }}</h3>
            <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing.{{ description }}</p>
            <div class="justify-content-center h5 text-black">${{ price }}</div>
            <div class="custom-control-inline add-cart">
                {{#quantity}}
                <a href="#" class="btn minus">-</a>
                <input class="form-control quantity{{ id }} text-center" data-max="{{quantity}}" value="1">
                <a href="#" class="btn plus">+</a>
                <a href="#" class="btn btn-black ml-1 rounded-0 add" data-element="{{ id }}">Cart</a>
                {{/quantity}}
                {{^quantity}}
                <div class='alert alert-danger' role='alert'>Agotado</div>
                {{/quantity}}
            </div>
        </div>
    </div>
</div>
{{/data}}