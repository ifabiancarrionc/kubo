var tpl = null;
jQuery(document).ready(function ($) {
    $.get('/tpl/product.tpl', function (tpldata) {
        tpl = tpldata;
    });
    var plus = function () {
        $('.plus').on('click', function (e) {
            e.preventDefault();
            input = $(this).parent().find('input');
            quantity = input.val();
            max = input.data('max');
            if (quantity < max) {
                quantity++;
                input.val(quantity);
                input.trigger('change');
            }
        });
    };
    plus();
    var minus = function () {
        $('.minus').on('click', function (e) {
            e.preventDefault();
            input = $(this).parent().find('input');
            quantity = input.val();
            if (quantity > 1) {
                quantity--;
                input.val(quantity);
                input.trigger('change');
            }
        });
    };
    minus();
    var paginate = function () {
        $('.pagination a').on('click', function (e) {
            e.preventDefault();
            if (!$(this).parent('li').hasClass('disabled')) {
                element = $(e);
                $.ajax({
                    url: $(this).attr('href'),
                    data: 'json',
                    method: 'get',
                    success: function (data) {
                        var rendered = Mustache.render(tpl, data);
                        $('.product-list').html(rendered);
                        $('.page-item').removeClass('active');
                        $('.page' + data.current_page).addClass('active');
                        if (data.next_page_url) {
                            $('.page-next').removeClass('disabled').find('page-item').attr('href', data.next_page_url);
                        } else {
                            $('.page-next').addClass('disabled');
                        }
                        if (data.prev_page_url) {
                            $('.page-before').removeClass('disabled').find('page-item').attr('href', data.prev_page_url);
                        } else {
                            $('.page-before').addClass('disabled');
                        }
                        plus();
                        minus();
                        add();
                        review();
                    }
                });
            }
        })
    };
    paginate();
    var add = function () {
        $('.add').on('click', function (e) {
            e.preventDefault();
            if ($('.quantity' + $(this).data('element')).val()) {
                $.ajax({
                    url: '/api/v1/cart/add',
                    method: 'post',
                    data: {
                        element: $(this).data('element'),
                        quantity: $('.quantity' + $(this).data('element')).val(),
                        "_token": $('#token').val()
                    },
                    dataType: 'json',
                    success: function (response) {
                        if (response.success) {
                            for (key in response.data) {
                                $('.element' + key + ' .add-cart').html("<div class='alert alert-success' role='alert'>Adicionado al carrito</div>");
                            }
                        }
                    }
                });
            }
        });
    };
    add();
    var review = function () {
        $.ajax({
            url: '/api/v1/cart/review',
            method: 'post',
            data: {
                element: $(this).data('element'),
                quantity: $('.quantity' + $(this).data('element')).val(),
                "_token": $('#token').val()
            },
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    for (key in response.data) {
                        $('.element' + key + ' .add-cart').html("<div class='alert alert-success' role='alert'>Adicionado al carrito</div>");
                    }
                }
            }
        });
    };
    var changeQuantity = function () {
        $('.quantity').on('change', function () {
            $.ajax({
                url: '/api/v1/cart/update',
                method: 'put',
                data: {
                    element: $(this).data('element'),
                    quantity: $(this).val(),
                    "_token": $('#token').val()
                },
                dataType: 'json',
                success: function (response) {
                    if (response.success) {
                        for (key in response.data.products) {
                            quantity = response.data.products[key].quantity;
                            price = response.data.products[key].price;
                            total = quantity*price;
                            $('#price' + key).html(price);
                            $('#total' + key).html(total);
                        }
                        $('#total').html(response.data.total);
                    }
                }
            });
        });
    };
    changeQuantity();
    var deleteElement = function () {
        $('.delete').on('click', function (e) {
            e.preventDefault();
            element = $(this).data('element');
            if (window.confirm('¿Desea quitarlo del carro?')) {
            console.log('esta haciendo algo esta mierda');
                $.ajax({
                    url: '/api/v1/cart/delete',
                    method: 'delete',
                    data: {
                        element: element,
                        "_token": $('#token').val()
                    },
                    dataType: 'json',
                    success: function (response) {
                        if (response.success) {
                            $('#element' + element).remove();
                            for (key in response.data.products) {
                                $('#price' + key).html(response.data.products[key].price);
                                $('#quantity' + key).html(response.data.products[key].quantity);
                                $('#total' + key).html(response.data.products[key].quantity * response.data.products[key].price);
                            }
                            $('#total').html(response.data.total);
                        }
                    }
                });
            }
        });
    };
    deleteElement();
    var clear = function () {
        $('.clear').on('click', function (e) {
            e.preventDefault();
            if (window.confirm('¿Desea vaciar el carro?')) {
                $.ajax({
                    url: '/api/v1/cart/clear',
                    method: 'delete',
                    data: {
                        "_token": $('#token').val()
                    },
                    dataType: 'json',
                    success: function (response) {
                        if (response.success) {
                            $('.element').remove();
                            $('#total').html(0);
                            $('.content').before("<div class='alert alert-success' role='alert'>Su compra se ha cancelado</div>");
                            $('.confirm').remove();
                            $('.clear').remove();
                            $('.loginConfirm').remove();
                        }
                    }
                });
            }
        });
    };
    clear();
    var confirm = function () {
        $('.confirm').on('click', function (e) {
            e.preventDefault();
            if (window.confirm('¿Desea confirmar su compra?')) {
                $.ajax({
                    url: '/api/v1/cart/confirm',
                    method: 'post',
                    data: {
                        "_token": $('#token').val()
                    },
                    dataType: 'json',
                    success: function (response) {
                        if (response.success) {
                            $('.element').remove();
                            $('#total').html(0);
                            $('.content').before("<div class='alert alert-success' role='alert'>Hemos recibido su solicitud y será procesada por nuestros agentes. Gracias por su compra</div>");
                            $('.confirm').remove();
                            $('.clear').remove();
                            $('.loginConfirm').remove();
                        }
                    }
                });
            }
        });
    };
    confirm();
});

