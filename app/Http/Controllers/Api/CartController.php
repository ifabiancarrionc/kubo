<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Sale;

class CartController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addElement(Request $request) {
        $cart = json_decode($request->session()->get('cart', '{}'), true);
        $cart[$request->get('element')] = $request->get('quantity');
        $request->session()->put('cart', json_encode($cart));
        $response = ["success" => true, "data" => $cart];
        return json_encode($response);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function review(Request $request) {
        $cart = json_decode($request->session()->get('cart', '{}'), true);
        $response = ["success" => true, "data" => $cart];
        return json_encode($response);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateElement(Request $request) {
        $cart['total'] = 0;
        $currentCart = json_decode($request->session()->get('cart', '{}'), true);
        $currentCart[$request->get('element')] = $request->get('quantity');
        $request->session()->put('cart', json_encode($currentCart));
        $cart = Product::getProductsData($currentCart);
        $response = ["success" => true, "data" => $cart];
        return json_encode($response);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteElement(Request $request) {
        $cart['total'] = 0;
        $currentCart = json_decode($request->session()->get('cart', '{}'), true);
        unset($currentCart[$request->get('element')]);
        $request->session()->put('cart', json_encode($currentCart));
        $cart = Product::getProductsData($currentCart);
        $response = ["success" => true, "data" => $cart];
        return json_encode($response);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function clear(Request $request) {
        $request->session()->remove('cart');
        $response = ["success" => true];
        return json_encode($response);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function confirm(Request $request) {

        $cart = json_decode($request->session()->get('cart', '{}'), true);
        $sale = new Sale();
        $sale->setCart($cart);
        $response = ["success" => true];
        return json_encode($response);
    }

}
