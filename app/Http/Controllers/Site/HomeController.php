<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;

class HomeController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $products = Product::paginate(9);
        $products->withPath('/products');
        $cart = json_decode($request->session()->get('cart', '{}'), true);
        return view('site.index', ['products' => $products, 'cart' => $cart]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function cart(Request $request) {
        $message = '';
        $cart = json_decode($request->session()->get('cart', '{}'), true);
        if (count($cart)) {
            $cart = Product::getProductsData($cart);
        } else {
            $message = "Su carrito de compras esta vacio";
        }
        return view('site.cart', ['cart' => $cart, 'message' => $message]);
    }

}
