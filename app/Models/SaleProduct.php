<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SaleProduct extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sale_product';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['quantity','price','sale_id','product_id'];

    public function sale() {
        return $this->belongsTo('App\Models\Sale');
    }
    public function product() {
        return $this->belongsTo('App\Models\Product');
    }
}