<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sale';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['total'];

    public function saleProducts() {
        return $this->hasMany('App\Models\SaleProduct');
    }

    public function setCart($cart) {
        $data = Product::getProductsData($cart);
        $this->total = $data['total'];
        $this->save();
        foreach ($data['products']as $key => $product) {
            $this->saleProducts()->create(["quantity" => $product['quantity'], 'product_id' => $key, "price" => $product['data']['price']]);
        }
    }

}
