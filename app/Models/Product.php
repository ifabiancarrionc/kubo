<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','description','price','image','quantity','category_id'];

    public function saleProducts() {
        return $this->hasMany('App\Models\SaleProduct');
    }

    public function category() {
        return $this->belongsTo('App\Models\Category');
    }

    public static function getProductsData($currentCart) {
        $cart = ["products"=>[],"total"=>0];
        foreach ($currentCart as $key => $quantity) {
            $product = Product::find($key);
            $cart['products'][$key] = ['quantity' => $quantity, 'price' => $product->price, 'data' =>$product->toArray()];
            $cart['total'] += $product->price * $quantity;
        }
        return $cart;
    }
}